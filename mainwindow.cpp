#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "stack_tab.cpp"
#include "queue.cpp"
#include <fstream>
#include <stack>
#include <ctime>
static stack <int> stosik;
std::stack <int> stosstd;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_create_stack_button_clicked()
{
    if (stosik.isEmpty() == true)
        ui->top_out->setText("Pusty");
    else
        ui->top_out->setText(QString::number(stosik.top()));
}

void MainWindow::on_pushButton_clicked()
{
    stosik.push(ui->stack_input->text().toInt());
}


void MainWindow::on_pushButton_2_clicked()
{
    ui->pop_out->setText(QString :: number(stosik.pop()));
}

void MainWindow::on_clr_button_clicked()
{
    stosik.clr();
}

void MainWindow::on_pushButton_4_clicked()
{
    stosstd.push(ui->stack_input_2->text().toInt());
}

void MainWindow::on_create_stack_button_2_clicked()
{
    if (stosstd.size() == 0)
        ui->top_out_2->setText("Pusty");
    else
        ui->top_out_2->setText(QString::number(stosstd.top()));
}

void MainWindow::on_pushButton_3_clicked()
{
    ui->pop_out_2->setText(QString :: number(stosstd.top()));
    stosstd.pop();
}

void MainWindow::on_clr_button_2_clicked()
{
    while(stosstd.empty() == false)
    {
        stosstd.pop();
    }
}

void MainWindow::on_load_stack_clicked()
{
    clock_t t1, t2;
    t1 = t2 = clock();
    for(int j=0;j<ui->liczba_el->text().toInt();j++)
        stosik.push(rand());
    while(t1 == t2)
        t2 = clock();

    ui->time1->setText(QString :: number((double)(t2-t1)/1000));
}







void MainWindow::on_loadstlbtt_clicked()
{
    clock_t t1, t2;
    t1 = t2 = clock();
    for(int j=0;j<ui->liczba_el_2->text().toInt();j++)
        stosstd.push(rand());
    while(t1 == t2)
        t2 = clock();

    ui->time1_2->setText(QString :: number((double)(t2-t1)/1000));

}

void MainWindow::on_savebt_clicked()
{
    stosik.save();

}