#ifndef STACK_TAB_HH
#define STACK_TAB_HH
#define SZABLON template <typename C>
SZABLON
class stack
{
    unsigned int cursor;
    C *wsk;
public:
    void push(C e);
    C pop();
    C top();
    void clr();
    stack();
    bool isEmpty();
    void save();
};


#endif // STACK_TAB_HH
